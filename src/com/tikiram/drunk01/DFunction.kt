package com.tikiram.drunk01






class SentenceMasm(
        line : Int,
        column: Int,
        val masm: String
)
    :Sentence(line, column)
{
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
    }

    override fun generateAssembly(ambit: Ambit): String {
        return "\t" + masm + "\n"
    }


}








class ExprNeg(
        line : Int,
        column: Int,
        val expr : Expr
)
    : Expr(line, column){
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr.check(parentAmbit, errors)
    }

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr.generateAssembly(ambit)
        assembly += "\tpop ax \n"
        assembly += "\tcwd\n"
        assembly += "\tmov cx, -1 \n"
        assembly += "\timul cx \n"
        assembly += "\tpush ax \n"
        return assembly
    }
}


class ExprMul(
        line : Int,
        column: Int,
        expr1 : Expr,
        expr2 : Expr
)
    : ExprBinary(line, column, expr1, expr2){


    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr1.generateAssembly(ambit)
        assembly += expr2.generateAssembly(ambit)
        assembly += "\tpop cx \n"
        assembly += "\tpop ax \n"
        assembly += "\tcwd\n"
        assembly += "\timul cx \n"
        assembly += "\tpush ax \n"
        return assembly
    }
}

class ExprDiv(
        line : Int,
        column: Int,
        expr1 : Expr,
        expr2 : Expr
)
    : ExprBinary(line, column, expr1, expr2){


    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr1.generateAssembly(ambit)
        assembly += expr2.generateAssembly(ambit)
        assembly += "\tpop cx \n"
        assembly += "\tpop ax \n"
        assembly += "\tcwd\n"
        assembly += "\tidiv cx \n"
        assembly += "\tpush ax \n"
        return assembly
    }
}

class ExprModule(
        line : Int,
        column: Int,
        expr1 : Expr,
        expr2 : Expr
)
    : ExprBinary(line, column, expr1, expr2){

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr1.generateAssembly(ambit)
        assembly += expr2.generateAssembly(ambit)
        assembly += "\tpop cx \n"
        assembly += "\tpop ax \n"
        assembly += "\tcwd\n"
        assembly += "\tidiv cx \n"
        assembly += "\tpush dx \n"
        return assembly
    }
}


class ExprAdd(
        line : Int,
        column: Int,
        expr1 : Expr,
        expr2 : Expr
)
    : ExprBinary(line, column, expr1, expr2){


    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr1.generateAssembly(ambit)
        assembly += expr2.generateAssembly(ambit)
        assembly += "\tpop cx \n"
        assembly += "\tpop dx \n"
        assembly += "\tadd cx, dx \n"
        assembly += "\tpush cx \n"
        return assembly
    }
}


class ExprSub(
        line : Int,
        column: Int,
        expr1 : Expr,
        expr2 : Expr
)
    : ExprBinary(line, column, expr1, expr2){

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr1.generateAssembly(ambit)
        assembly += expr2.generateAssembly(ambit)
        assembly += "\tpop dx \n"
        assembly += "\tpop cx \n"
        assembly += "\tsub cx, dx \n"
        assembly += "\tpush cx \n"
        return assembly
    }
}

class ExprTime3(
        line : Int,
        column: Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
    }
    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += "\tmov  ah, 2ch\n"
        assembly += "\tint  21h\n"
        assembly += "\txor bx, bx\n"
        assembly += "\tmov bl, dh\n"
        assembly += "\tpush bx\n"
        return assembly
    }
}
class ExprTime2(
        line : Int,
        column: Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
    }
    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += "\tmov  ah, 2ch\n"
        assembly += "\tint  21h\n"
        assembly += "\txor bx, bx\n"
        assembly += "\tmov bl, cl\n"
        assembly += "\tpush bx\n"
        return assembly
    }
}
class ExprTime1(
        line : Int,
        column: Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
    }
    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += "\tmov  ah, 2ch\n"
        assembly += "\tint  21h\n"
        assembly += "\txor bx, bx\n"
        assembly += "\tmov bl, ch\n"
        assembly += "\tpush bx\n"
        return assembly
    }
}

class ExprOpenFileForRead(
        line : Int,
        column: Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        // nada lol
    }

    override fun generateAssembly(ambit: Ambit): String {

        val kc = Driver.getKeyCount()

        val labelFalse = "_openFileFail$kc"
        val labelOut = "_openFileOut$kc"

        var assembly = ""
//        assembly += "\tMOV AH, 3CH\n"
        assembly += "\tMOV AH, 3DH\n"
        assembly += "\tMOV CX, 6\n"
        assembly += "\tLEA DX, asgard\n"
        assembly += "\tINT 21H \n"
        assembly += "\tjc $labelFalse\n"
        assembly += "\tpush ax\n"
        assembly += "\tjmp $labelOut\n"
        assembly += "\t$labelFalse:\n"
        assembly += "\tmov cx, 0\n"
        assembly += "\tpush cx\n"
        assembly += "\t$labelOut:\n"
        return assembly
    }
}

class ExprOpenFile(
        line : Int,
        column: Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        // nada lol
    }

    override fun generateAssembly(ambit: Ambit): String {

        val kc = Driver.getKeyCount()

        val labelFalse = "_openFileFail$kc"
        val labelOut = "_openFileOut$kc"

        var assembly = ""
        assembly += "\tMOV AH, 3CH\n"
//        assembly += "\tMOV AH, 3DH\n"
        assembly += "\tMOV CX, 6\n"
        assembly += "\tLEA DX, asgard\n"
        assembly += "\tINT 21H \n"
        assembly += "\tjc $labelFalse\n"
        assembly += "\tpush ax\n"
        assembly += "\tjmp $labelOut\n"
        assembly += "\t$labelFalse:\n"
        assembly += "\tmov cx, 0\n"
        assembly += "\tpush cx\n"
        assembly += "\t$labelOut:\n"
        return assembly
    }
}


class ExprWaitKey(
        line : Int,
        column: Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        // nada lol
    }

    override fun generateAssembly(ambit: Ambit): String {

        val kc = Driver.getKeyCount()

//        val labelGetKey = "_getKey$kc"
//        val labelknone = "_Knone$kc"
//        val labelkout = "_keyout$kc"

        var assembly = ""
        assembly += "\tmov ah,0h\n" // ;Get the key that was pressed!
        assembly += "\tint 16h\n"
        assembly += "\txor ah, ah\n"
        assembly += "\tpush ax\n"
        return assembly
    }
}

class ExprKey(
        line : Int,
        column: Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        // nada lol
    }

    override fun generateAssembly(ambit: Ambit): String {

        val kc = Driver.getKeyCount()

        val labelGetKey = "_getKey$kc"
        val labelknone = "_Knone$kc"
        val labelkout = "_keyout$kc"

        var assembly = ""
        assembly += "\tmov ah, 1h\n" // ;Check if any key was pressed in the keyboard!
        assembly += "\tint 16h\n"
        assembly += "\tjnz $labelGetKey\n" // ; key was pressed!
        assembly += "\tjmp $labelknone\n" // ;key wasent pressed!
        assembly += "\t$labelGetKey:\n"
        assembly += "\tmov ah,0h\n" // ;Get the key that was pressed!
        assembly += "\tint 16h\n"

        assembly += "\tpush ax\n"
        assembly += "\tjmp $labelkout\n"
        assembly += "\t$labelknone: \n"
        assembly += "\tmov cx, 0\n"
        assembly += "\tpush cx\n"
        assembly += "\t$labelkout:\n"
        return assembly
    }
}


class ExprChar(
        line : Int,
        column: Int,
        val string : String
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        // podria validar el maximo numero de un entero
    }

    override fun generateAssembly(ambit: Ambit): String {
        val ascii = string[0].toInt()
        var assembly = ""
        assembly += "\tmov cx, $ascii \n"
        assembly += "\tpush cx \n"
        return assembly
    }
}
class ExprInteger(
        line : Int,
        column: Int,
        val value : Int
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        // podria validar el maximo numero de un entero
    }

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += "\tmov cx, $value \n"
        assembly += "\tpush cx \n"
        return assembly
    }
}


abstract class Expr(
        line: Int,
        column: Int
) : Sentence(line, column) {
    var exprType : String = Driver.INT_TYPE_KEY
    var isArray : Boolean = false
    var hasDirection = false
}

abstract class ExprBinary(
        line: Int,
        column : Int,
        val expr1 : Expr,
        val expr2 : Expr
) : Expr(line, column)
{
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr1.check(parentAmbit, errors)
        expr2.check(parentAmbit, errors)
    }
}
