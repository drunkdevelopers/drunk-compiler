package com.tikiram.drunk01

class ExprIdentifier(
        line : Int,
        column: Int,
        val name : String
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        val existedVar = parentAmbit.localVars[name]
        if (existedVar != null){
            exprType = existedVar.type
            isArray = existedVar.isArray
            hasDirection = true
        }
        else {
            errors.add(SemanticError(this, message = "Variable '${name}' no declarada"))
        }
    }

    override fun generateAssembly(ambit: Ambit): String {
        val localVar = ambit.localVars[name]!!
        val type = ambit.sets[localVar.type]!!
        var assembly = ""

        assembly += "\tmov cx, bp\n"
        if (localVar.position < 0) {
            assembly += "\tsub cx, ${-localVar.position}\n"
        }
        else {
            assembly += "\tadd cx, ${localVar.position}\n"
        }

        assembly += "\tmov bx, cx\n"  // guardo la direccion en bx

        // Si es un parametro y no es primitivo, lo que tiene como valor es una referencia
        if (localVar.position > 0 && (localVar.isArray || !type._isPrimitive)){
            assembly += "\tmov si, ss:[bx]\n"
            assembly += "\tmov bx, si\n"
            assembly += "\tpush bx\n"
        }
        else {
            // Si es primitivo y no es un array, sacar su valor
            if (type._isPrimitive && !localVar.isArray){
                assembly += "\tmov cx, ${localVar.bpPosition}\n"
                assembly += "\tpush cx \n"
            }
            else {
                assembly += "\tpush cx \n"
            }

        }



        return assembly
    }
}