package com.tikiram.drunk01

class ExprDot(
        line : Int,
        column: Int,
        val expr : Expr,
        val name : String
) : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr.check(parentAmbit, errors)

        hasDirection = true

        if (!expr.isArray){
            if (expr.hasDirection) {
                val type = parentAmbit.sets[expr.exprType]!!
                if (type._isPrimitive){
                    errors.add(SemanticError(this, message = "Tipo ${type.name} de tipo primitivo no tiene atributos"))
                }
                else {
                    val existedVar = type.localVars[name]
                    if (existedVar != null){
                        // todo bien
                    }
                    else {
                        errors.add(SemanticError(this, message = "Tipo ${type.name} no tiene propiedad llamada '${name}'"))
                    }
                }
            }
            else {
                errors.add(SemanticError(this, message = "No es una direccion valida de memoria como para aplicar puntero"))
            }
        }
        else {
            errors.add(SemanticError(this, message = "Los arrays no tiene atributos aqui"))
        }

    }

    override fun generateAssembly(ambit: Ambit): String {

        val type = ambit.sets[expr.exprType]!!
        val existedVar = type.localVars[name]!!

        var assembly = ""
        assembly += expr.generateAssembly(ambit) // direccion
        assembly += "\tpop cx \n" // saco el indice o valor y lo ignoro, index esta en bx
        assembly += "\tadd bx, ${existedVar.position} \n" // obtengo la direccion de propiedad

        // este tipo siempre sera primitivo
        assembly += "\tmov cx, ss:[bx] \n" // obttengo el valor
        assembly += "\tpush cx \n" // guardo el valor

        return assembly
    }
}