package com.tikiram.drunk01

import java.io.BufferedReader
import java.io.File
import java.io.StringReader
import java.util.concurrent.TimeUnit
import com.sun.xml.internal.ws.streaming.XMLStreamReaderUtil.close
import java.io.PrintWriter
import java.io.FileReader



//* Completar las referencias


object Main {

    @JvmStatic
    fun main(args: Array<String>) {

        if (args.size > 1){

            val path = args[0]

            var contenido = ""

            BufferedReader(FileReader(path)).use { br ->
                val sb = StringBuilder()
                var line: String? = br.readLine()

                while (line != null) {
                    sb.append(line)
                    sb.append(System.lineSeparator())
                    line = br.readLine()
                }
                contenido = sb.toString()
            }

            var s: Scanner? = null
            var p: Parser? = null

            try {
                s = Scanner(BufferedReader(StringReader(contenido)))
                p = Parser(s)
                p.parse()

                p.driver.check()

                for (e in p.driver.semanticErrors){

                    System.out.println("Linea: ${e.sentence?.line ?: 0}, Columna: ${e.sentence?.column ?: 0} : ${e.message} ")
                }

                if (p.driver.semanticErrors.isEmpty()){
                    val generated = p.driver.generateAssembly()

//                File("c:\\MASM\\game.asm").printWriter().use { out ->
//                    out.println(generated)
//                }

                    val writer = PrintWriter(args[1], "UTF-8")
                    writer.println(generated)
                    writer.close()


//                val proc = ProcessBuilder("c:\\MASM\\compilar2.bat")
//                        .redirectOutput(ProcessBuilder.Redirect.PIPE)
//                        .redirectError(ProcessBuilder.Redirect.PIPE)
//                        .start()
//
//                proc.waitFor(60, TimeUnit.MINUTES)
//                val result = proc.inputStream.bufferedReader().readText()
//
//                System.out.println(result)

                    System.out.println("Archivo Generado")
                }



                println("Creo que todo bien")

            } catch (ex: Exception) {
                println(ex.message)
                ex.printStackTrace()
            }

        }
        else {
            println("Tienes que especificar el path del archivo y el path destino  >:(")
        }

    }
}
