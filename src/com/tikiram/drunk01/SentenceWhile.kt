package com.tikiram.drunk01

class SentenceWhile(
        line: Int,
        column: Int,
        val expr: Expr,
        val sentences: List<Sentence>
)    :Sentence(line, column)
{
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr.check(parentAmbit, errors)
        sentences.forEach { it.check(parentAmbit, errors) }
    }

    override fun generateAssembly(ambit: Ambit): String {

        val cc = Driver.getWhileCount()
        val label = "_while$cc"
        val labelOut = "_whileOut$cc"

        var assembly = ""
        assembly += "\t$label: \n"
        assembly += expr.generateAssembly(ambit)
        assembly += "\tpop cx \n"
        assembly += "\tmov dx, 0\n"
        assembly += "\tcmp cx, dx \n"
        assembly += "\tje $labelOut  \n"
        sentences.forEach {
            assembly += it.generateAssembly(ambit)
        }
        assembly += "\tjmp $label\n"
        assembly += "\t$labelOut:  \n"
        return assembly
    }
}