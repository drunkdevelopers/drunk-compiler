package com.tikiram.drunk01

class SentenceAssignation(
        line: Int,
        column: Int,
        val name: String,
        val expr: Expr
)    :Sentence(line, column)
{
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr.check(parentAmbit, errors)
        val existedVar = parentAmbit.localVars[name]
        if (existedVar == null){
            errors.add(SemanticError(this, message = "Variable '${name}' no declarada"))
        }
    }

    override fun generateAssembly(ambit: Ambit): String {
        val localVar = ambit.localVars[name]!!
        var assembly = ""
        assembly += expr.generateAssembly(ambit)
        assembly += "\tpop cx \n"
        assembly += "\tmov ${localVar.bpPosition}, cx \n"
        return assembly
    }
}