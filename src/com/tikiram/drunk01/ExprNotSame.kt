package com.tikiram.drunk01

class ExprNotSame(
        line : Int,
        column: Int,
        expr1 : Expr,
        expr2 : Expr
)
    : ExprBinary(line, column, expr1, expr2){

    override fun generateAssembly(ambit: Ambit): String {

        val cc = Driver.getComparatorCount()
        val label = "_comp$cc"
        val labelOut = "_compOut$cc"

        var assembly = ""
        assembly += expr1.generateAssembly(ambit)
        assembly += expr2.generateAssembly(ambit)
        assembly += "\tpop dx \n"
        assembly += "\tpop cx \n"
        assembly += "\tcmp cx, dx \n"
        assembly += "\tjne $label \n"
        assembly += "\tmov cx, 0 \n"
        assembly += "\tpush cx \n"
        assembly += "\tjmp $labelOut \n"
        assembly += "\t$label: \n"
        assembly += "\tmov cx, 1\n"
        assembly += "\tpush cx\n"
        assembly += "\t$labelOut:\n"
        return assembly
    }
}