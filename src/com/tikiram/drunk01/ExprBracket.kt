package com.tikiram.drunk01

class ExprBracket(
        line : Int,
        column: Int,
        val expr1 : Expr,
        val expr2 : Expr
)
    : Expr(line, column){

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr1.check(parentAmbit, errors)
        expr2.check(parentAmbit, errors)
        if (!expr1.isArray){
            errors.add(SemanticError(this,message = " Expresión no es un array. "))
        }

        // actualizo el tipo
        isArray = false
        exprType = expr1.exprType
        hasDirection = true
    }

    override fun generateAssembly(ambit: Ambit): String {

        val type = ambit.sets[expr1.exprType]!!

        var assembly = ""
        assembly += expr1.generateAssembly(ambit) // direccion
        assembly += expr2.generateAssembly(ambit) // indice
        assembly += "\tpop ax \n" // saco el indice
        assembly += "\tcwd \n" // completo dx:ax
        assembly += "\tmov cx, ${type.size} \n" // guardo el tamaño del tipo
        assembly += "\timul cx \n" // se multiplica el indice con el tamaño del tipo, ax = result

        assembly += "\tpop cx \n" // saco la direccion
        assembly += "\tadd cx, ax \n" // calculo la direccion del elemento

        assembly += "\tmov bx, cx \n" // guardo la direccion en bx

        // entonces ya teniendo la direccion, si es primitivo, muestro su valor
        // si no es primitivo, retorno su direccion


        if (type._isPrimitive){
            assembly += "\tmov cx, ss:[bx] \n" // obttengo el valor
            assembly += "\tpush cx \n" // guardo el valor
        }
        else {
            assembly += "\tpush cx \n" // guardo la direccion
        }

        return assembly

    }
}


class ExprFile(
        line : Int,
        column: Int,
        val expr2 : Expr
)
    : Expr(line, column){

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr2.check(parentAmbit, errors)
    }

    override fun generateAssembly(ambit: Ambit): String {

        var assembly = ""
        assembly += expr2.generateAssembly(ambit) // indice

//        assembly += "\tpop cx \n" // saco el indice
//        assembly += "\tlea si, imagen \n" // saco el indice
//        assembly += "\tadd si, cx \n" // saco el indice
//        assembly += "\tmov cl,[si] \n" // saco el indice
//        assembly += "\tmovzx cx, cl \n" // saco el indice
//        assembly += "\tpush cx\n" // saco el indice
//

        assembly += "\tpop bx \n" // saco el indice
        assembly += "\txor cx, cx \n" // saco el indice
        assembly += "\tmov cl, asgard[bx] \n" // saco el indice
//        assembly += "\tmovzx cx, cl \n" // saco el indice
        assembly += "\tpush cx\n" // saco el indice

        return assembly

    }
}