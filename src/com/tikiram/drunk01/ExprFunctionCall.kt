package com.tikiram.drunk01

class ExprFunctionCall(
        line : Int,
        column : Int,
        val name : String,
        val args : List<Expr>
)
    : Expr(line, column) {

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {

        for (arg in args){
            arg.check(parentAmbit, errors)
        }

        val dFunction = parentAmbit.dFunctions[name]
        if (dFunction != null){
            if (args.size == dFunction.parameters.size){

                var c = 0
                while (c < args.size){
                    val arg = args[c]
                    val par = dFunction.parameters[c]
                    if (arg.exprType != par.type){
                        errors.add(SemanticError(this,message = "$name : Argumento incorrecto: '${arg.exprType}' != '${par.type}' "))
                    }
                    c++
                }
            }
            else {
                errors.add(SemanticError(this,message = "Funcion '$name' tiene diferente cantidad de parametros"))
            }
        }
        else {
            errors.add(SemanticError(this,message = "No se tiene definida una funcion '$name'"))
        }
    }

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        for (arg in args){
            assembly += arg.generateAssembly(ambit)
        }
        assembly += "\tcall $name \n"
        return assembly
    }
}
