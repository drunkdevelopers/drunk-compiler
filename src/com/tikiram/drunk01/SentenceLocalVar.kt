package com.tikiram.drunk01

class SentenceLocalVar(
        line: Int,
        column: Int,
        val type: String,
        val name: String,
        val isArray: Boolean = false,
        val exprInteger : ExprInteger = ExprInteger(0,0,1)
)
    :Sentence(line, column)
{

    var isParameter = false
    var position : Int = 0

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        exprInteger.check(parentAmbit, errors)
        if (exprInteger.value <= 0){
            errors.add(SemanticError(this, "Valor '${exprInteger.value}' invalido, tiene que ser mayor a 0"))
        }

        val existedVar = parentAmbit.localVars[name]
        if (existedVar == null){
            val type = parentAmbit.sets[type]
            if (type != null){
                parentAmbit.localVars[name] = this
            }
            else {
                errors.add(SemanticError(this, "Tipo '${type}' no definido"))
            }
        }
        else {
            errors.add(SemanticError(this, message = "Variable '${name}' ya declarada"))
        }
    }



    val bpPosition : String
    get() {
        return if (position < 0){
            "[bp-${-position}]"
        }
        else {
            "[bp+${position}]"
        }
    }


    override fun generateAssembly(ambit: Ambit): String {
        val size = ambit.sets[type]!!.size
        val total = size * exprInteger.value

        return "\tsub sp, ${total}\n"
    }
}