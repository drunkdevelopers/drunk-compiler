package com.tikiram.drunk01;

import java_cup.runtime.Symbol;
import java.util.ArrayList;

%%

%class Scanner
%cupsym SYM
%cup
%public
%unicode
%line
%column
%char
%ignorecase

%{
    public ArrayList<Falla> fallas = new ArrayList<>();

    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn, new Coordenada(yytext(), yyline + 1, yycolumn + 1) );
    }
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }

    private String stringContent(String string){
        return string.substring(1, string.length() - 1);
    }

%}

integer         = [0-9]+
double          = {integer}+"."{integer}+
string          = \"[^\"]*\"
LineTerminator  = \r|\n|\r\n
WhiteSpace      = {LineTerminator} | [ \t\f]
Identifier      = [:jletter:] [:jletterdigit:]*

BName           = "["[ ]*[:jletter:] [:jletterdigit:]*[[:jletter:] [:jletterdigit:]| ]*"]"
//BName                = "[" Identi

Comment              = {TraditionalComment} | {EndOfLineComment}
InputCharacter       = [^\r\n]
TraditionalComment   = "¿¡" [^¡] ~"¡?" | "¿¡" "¡"+ "?"
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}?

%%

<YYINITIAL>{

    "Proyecto"      { return symbol(SYM.KEYPROYECTO); }
    "Context"       { return symbol(SYM.KEYCONTEXT); }
    {BName}         { return symbol(SYM.BNAME, stringContent(yytext())); }
    "{"             { return symbol(SYM.LBRACE); }
    "}"             { return symbol(SYM.RBRACE); }
    ";"             { return symbol(SYM.SEMICOLON); }
    "="             { return symbol(SYM.EQUALS); }

    "+"     { return symbol(SYM.PLUS); }
    "-"     { return symbol(SYM.MINUS); }
    "/"     { return symbol(SYM.DIVIDE); }
    "*"     { return symbol(SYM.TIMES); }
    
    "Rq"        { return symbol(SYM.KEYRQ); }
    "Ptn"       { return symbol(SYM.KEYPTN); }
    "printer"   { return symbol(SYM.KEYPRINTER); }
    "VarRef"    { return symbol(SYM.KEYVARREF); }
    "("         { return symbol(SYM.LPAREN); }
    ")"         { return symbol(SYM.RPAREN); }
    ","             { return symbol(SYM.COMMA); }

    {integer}       { return symbol(SYM.INTEGER, Integer.valueOf(yytext())); }
    {double}        { return symbol(SYM.DOUBLE,  Double.valueOf(yytext())); }
    {string}        { return symbol(SYM.STRING, stringContent(yytext())); }

    "Var"           { return symbol(SYM.KEYVAR); }

    {Identifier}    { return symbol(SYM.IDENTIFIER);}



    {Comment}       {}
    {WhiteSpace}    {}
    .               { fallas.add(new Falla(yytext(), yyline + 1, yycolumn + 1)); }
}
