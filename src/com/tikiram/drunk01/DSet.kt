package com.tikiram.drunk01

class DSet(
        line : Int,
        column: Int,
        val name: String,
        private val sentences: List<SentenceLocalVar>,
        _bytes : Int = 0,
        val _isPrimitive: Boolean = false
) : Sentence(line, column)
{
    val localVars = HashMap<String, SentenceLocalVar>()

    private var localVarCount = _bytes

    val size : Int
    get() = localVarCount

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {

        for (sentence in sentences) {
            val existedVar = localVars[sentence.name]
            if (existedVar == null){

                val type = parentAmbit.sets[sentence.type]
                if (type != null){
                    if (type._isPrimitive) {
                        localVars[sentence.name] = sentence
                        sentence.position = localVarCount
                        localVarCount += type.size
                    }
                    else {
                        errors.add(SemanticError(sentence, "Tipo '${sentence.type}' no primitivo"))
                    }
                }
                else {
                    errors.add(SemanticError(sentence, "Tipo '${sentence.type}' no definido"))
                }
            }
            else {
                errors.add(SemanticError(sentence, message = "Variable '${sentence.name}' ya declarada"))
            }
        }

    }

    override fun generateAssembly(ambit: Ambit): String {
        return ";--"
    }
}