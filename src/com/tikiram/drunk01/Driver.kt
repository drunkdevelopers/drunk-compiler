package com.tikiram.drunk01

class Driver {

    private val ambit = Ambit()

    //val dFunctions = HashMap<String, DFun>()
    val semanticErrors  = mutableListOf<SemanticError>()

    companion object {
        val INT_TYPE_KEY = "int"
        private var cc = 0
        private var ifc = 0
        private var wc = 0
        private var kc = 0

        fun getKeyCount() : Int {
            kc++
            return kc
        }

        fun getWhileCount() : Int{
            wc++
            return wc
        }

        fun getIfCount() : Int{
            ifc++
            return ifc
        }

        fun getComparatorCount() : Int{
            cc++
            return cc
        }
    }

    init {

        ambit.sets[INT_TYPE_KEY] = DSet(0,0,INT_TYPE_KEY, listOf(), 2, true)

        val SET_UPIXEL_KEY = "setUPixel"
        val setUPixel = getSetUPixelFun(SET_UPIXEL_KEY)
        ambit.dFunctions[SET_UPIXEL_KEY] = setUPixel

        val VIDEO_MODE_KEY = "videoMode"
        val videoMode = getVideoModeFun(VIDEO_MODE_KEY)
        ambit.dFunctions[VIDEO_MODE_KEY] = videoMode

        val PRINT_CHAR_KEY = "printChar"
        val printChar = getPrintCharFun(PRINT_CHAR_KEY)
        ambit.dFunctions[PRINT_CHAR_KEY] = printChar

        val PRINT_LINE_KEY = "printLine"
        val printLine = getPrintLineFun(PRINT_LINE_KEY)
        ambit.dFunctions[PRINT_LINE_KEY] = printLine

    }



    private fun getSetUPixelFun(key: String) : DFun{
        val parameters = listOf<SentenceLocalVar>(
                SentenceLocalVar(0,0, INT_TYPE_KEY, "pos"),
                SentenceLocalVar(0,0, INT_TYPE_KEY, "color")
        )
        val sentences = mutableListOf<Sentence>()
        sentences.add(SentenceMasm(0,0,"mov di, [bp+6]"))
        sentences.add(SentenceMasm(0,0,"mov dl, [bp+4]"))
        sentences.add(SentenceMasm(0,0,"mov es:[di], dl"))
        val setUPixel = DFun(0,0,key, parameters, sentences)
        return setUPixel
    }

    private fun getPrintCharFun(key: String) : DFun{
        val parameters = listOf<SentenceLocalVar>(SentenceLocalVar(0,0, INT_TYPE_KEY, "number"))
        val sentences = mutableListOf<Sentence>()
        sentences.add(SentenceMasm(0,0,"mov bx, offset buff"))
        sentences.add(SentenceMasm(0,0,"mov cx, [bp+4]"))
        sentences.add(SentenceMasm(0,0,"mov [bx], cl"))
        sentences.add(SentenceMasm(0,0,"mov ah, 09"))
        sentences.add(SentenceMasm(0,0,"lea dx, buff"))
        sentences.add(SentenceMasm(0,0,"int 21h"))
        val printChar = DFun(0,0,key, parameters, sentences)
        return printChar
    }

    private fun getPrintLineFun(key: String) : DFun{
        val sentences = mutableListOf<Sentence>()
        sentences.add(SentenceMasm(0,0,"mov ah, 09"))
        sentences.add(SentenceMasm(0,0,"lea dx, line"))
        sentences.add(SentenceMasm(0,0,"int 21h"))
        val printLine = DFun(0,0,key, listOf(), sentences)
        return printLine
    }

    private fun getVideoModeFun(key : String) : DFun{

        val sentences = mutableListOf<Sentence>()
        sentences.add(SentenceMasm(0,0,"mov ax, 13h"))
        sentences.add(SentenceMasm(0,0,"int 10h"))
        sentences.add(SentenceMasm(0,0,"mov ax, 0A000h"))
        sentences.add(SentenceMasm(0,0,"mov es, ax"))
        val videoMode = DFun(0,0,key, listOf(), sentences)
        return videoMode
    }

    fun addDSet(dSet: DSet){
        val dSetDefined = ambit.sets[dSet.name]
        if (dSetDefined == null){
            ambit.sets[dSet.name] = dSet
        }
        else {
            semanticErrors.add(SemanticError(dSet, "Ya existe un Set con el mismo nombre"))
        }
    }
    fun addDFun(dFun : DFun){

        val dFunDefined = ambit.dFunctions[dFun.name]

        if (dFunDefined == null){
            ambit.dFunctions[dFun.name] = dFun
        }
        else {
            semanticErrors.add(SemanticError(dFun, "Ya existe una funcion con el mismo nombre"))
        }
    }

    // gezz rick calm down
    // this method should check the consistency of the program
    fun check(){

        // check that the 'main' function exists
        val mainFunction = ambit.dFunctions["main"]
        if (mainFunction == null){
            semanticErrors.add(SemanticError(message = "No se tiene definida una funcion 'main'"))
        }

        // check the types
        for (dSet in ambit.sets.values){
            dSet.check(ambit, semanticErrors)
        }

        // check the functions // really cares?
        for (dFun in ambit.dFunctions.values){
            dFun.check(ambit, semanticErrors)
        }
    }


    fun generateAssembly() : String {

        var assembly = ""

        assembly += ".model small\n"
        assembly += ".stack 4095\n"
        assembly += ".data\n"

        //buff db 100 dup('$')

        assembly += "buff db 2 dup('\$')\n"
        assembly += "line db 10, 13, '\$'\n"
        assembly += "asgard db 1024 dup('\$')\n"
//        assembly += "asgard db \"reportehope.txt\", 0 \n"



        assembly += ".code\n"

        assembly += "mov ax,@data\n"
        assembly += "mov ds,ax\n"
        assembly += "call main\n"
        assembly += "jmp _out\n"


        for (dFun in ambit.dFunctions.values){
            assembly += dFun.generateAssembly(ambit)
        }

        assembly += "_out:\n"
        assembly += "mov ah, 4ch\n"
        assembly += "int 21h\n"
        assembly += "end\n"


        return assembly

    }

}

open class DrunkException(message: String) : Exception(message){

}

class Ambit(){
    val localVars = HashMap<String, SentenceLocalVar>()
    var sets = HashMap<String, DSet>()
    var dFunctions = HashMap<String, DFun>()
}

abstract class Sentence(
        val line : Int,
        val column: Int
){

    abstract fun generateAssembly(ambit: Ambit): String
    abstract fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>)

}




class SemanticError(
        val sentence: Sentence? = null,
        val message : String = "You fucked up"
){
}