package com.tikiram.drunk01

class SentenceFill(
        line: Int,
        column: Int,
        val exprIdentifier: ExprIdentifier,
        val string: String
)    :Sentence(line, column)
{

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        exprIdentifier.check(parentAmbit, errors)
        if (!exprIdentifier.isArray){
            errors.add(SemanticError(this, "Fill tiene que tener un destino array"))
        }
        if (exprIdentifier.exprType != Driver.INT_TYPE_KEY){
            errors.add(SemanticError(this, "Fill tiene que tener un destino de tipo array INT, no de tipo ${exprIdentifier.exprType}"))
        }

        val existedVar = parentAmbit.localVars[exprIdentifier.name]
        if (existedVar != null){
            if ((string.length + 1) > existedVar.exprInteger.value){
                errors.add(SemanticError(this, "Array ${exprIdentifier.name} de tamaño ${existedVar.exprInteger.value}, string de tamaño ${string.length}, el array tiene que tener una posicion extra"))
            }
        }

    }

    override fun generateAssembly(ambit: Ambit): String {
        val existedVar = ambit.localVars[exprIdentifier.name]!!
        val chars = string.toCharArray()
        var assembly = ""
        assembly += exprIdentifier.generateAssembly(ambit) // es de tipo array
        assembly += "\tpop bx\n" // saco su direccion de memoria
        for (i in 0 until existedVar.exprInteger.value){
            val byte = i * 2
            val ascii = if(i < chars.size) chars[i].toInt() else 0
            assembly += "\tmov cx, $ascii\n"
            assembly += "\tmov ss:[bx+$byte], cx\n"
        }
        return assembly
    }
}