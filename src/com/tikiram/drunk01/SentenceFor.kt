package com.tikiram.drunk01

class SentenceFor(
        line: Int,
        column: Int,
        val assignation1: SentenceAssignation,
        expr: Expr,
        assignation2: SentenceAssignation,
        sentences: List<Sentence>
)    :Sentence(line, column)
{
    val sentenceWhile : SentenceWhile

    init {
        val list = mutableListOf<Sentence>()
        list.addAll(sentences)
        list.add(assignation2)
        sentenceWhile = SentenceWhile(line, column, expr, list)
    }

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        assignation1.check(parentAmbit, errors)
        sentenceWhile.check(parentAmbit, errors)
    }

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += assignation1.generateAssembly(ambit)
        assembly += sentenceWhile.generateAssembly(ambit)
        return assembly
    }
}