package com.tikiram.drunk01

    class SentenceArrayAssignation(
        line: Int,
        column: Int,
        val exprBracket: ExprBracket,
        val expr: Expr
)    :Sentence(line, column)
{
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        exprBracket.check(parentAmbit, errors)
        expr.check(parentAmbit, errors)
        if (exprBracket.exprType != expr.exprType){
            errors.add(SemanticError(this, message = "Array y valor no son de tipos compatibles"))
        }
    }

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr.generateAssembly(ambit) // obtengo el valor
        assembly += exprBracket.generateAssembly(ambit) // bx tendra la direccion (primitivo o no)
        assembly += "\tpop cx \n" // retiro el valor o direccion pusheado de exprBrackeet
        assembly += "\tpop cx \n" // obtengo el valor de la expresion
        assembly += "\tmov ss:[bx], cx \n" // guardo el valor en la direccion
        return assembly
    }
}