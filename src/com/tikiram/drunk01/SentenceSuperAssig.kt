package com.tikiram.drunk01

class SentenceSuperAssig(
        line: Int,
        column: Int,
        val expr1: Expr,
        val expr2: Expr
)    :Sentence(line, column)
{
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr1.check(parentAmbit, errors)
        expr2.check(parentAmbit, errors)
        if (expr1.exprType != expr2.exprType){
            errors.add(SemanticError(this, message = "Tipos no compatibles"))
        }
        if (!expr1.hasDirection) {
            errors.add(SemanticError(this, message = "El destino no es una direccion valida"))
        }
        if (expr1.isArray){
            errors.add(SemanticError(this, message = "No se puede escribir sobre un array, olvido los []?"))
        }
    }

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += expr2.generateAssembly(ambit) // obtengo el valor
        assembly += expr1.generateAssembly(ambit) // bx tendra la direccion (primitivo o no)
        assembly += "\tpop cx \n" // retiro el valor o direccion pusheado de expr1
        assembly += "\tpop cx \n" // obtengo el valor de la expresion
        assembly += "\tmov ss:[bx], cx \n" // guardo el valor en la direccion
        return assembly
    }
}