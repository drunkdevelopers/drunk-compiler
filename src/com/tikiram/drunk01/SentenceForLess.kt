package com.tikiram.drunk01

class SentenceForLess(
        line: Int,
        column: Int,
        name1 : String,
        name2 : String,
        sentences: List<Sentence>
)    :Sentence(line, column)
{
    val sentenceFor : SentenceFor

    init {
        val assignation1 = SentenceAssignation(line, column, name1, ExprInteger(line, column, 0))
        val assignation2 = SentenceAssignation(line, column, name1, ExprAdd(line, column, ExprIdentifier(line, column, name1), ExprInteger(line, column, 1)))
        val expr = ExprLess(line, column, ExprIdentifier(line, column, name1), ExprIdentifier(line, column, name2))
        sentenceFor = SentenceFor(line, column, assignation1, expr, assignation2, sentences)
    }

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        sentenceFor.check(parentAmbit, errors)
    }

    override fun generateAssembly(ambit: Ambit): String {
        var assembly = ""
        assembly += sentenceFor.generateAssembly(ambit)
        return assembly
    }
}