package com.tikiram.drunk01

class SentenceIf(
        line: Int,
        column: Int,
        val expr: Expr,
        val trueSentences: List<Sentence>,
        val falseSentences: List<Sentence>
)    :Sentence(line, column)
{
    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        expr.check(parentAmbit, errors)
        trueSentences.forEach { it.check(parentAmbit, errors) }
        falseSentences.forEach { it.check(parentAmbit, errors) }
    }

    override fun generateAssembly(ambit: Ambit): String {

        val cc = Driver.getIfCount()
        val labelFalse = "_ifFalse$cc"
        val labelOut = "_ifOut$cc"

        var assembly = ""
        assembly += expr.generateAssembly(ambit)
        assembly += "\tpop cx \n"
        assembly += "\tmov dx, 0\n"
        assembly += "\tcmp cx, dx \n"
        assembly += "\tje $labelFalse  \n"
        trueSentences.forEach {
            assembly += it.generateAssembly(ambit)
        }
        assembly += "\tjmp $labelOut \n"
        assembly += "\t$labelFalse:  \n"
        falseSentences.forEach {
            assembly += it.generateAssembly(ambit)
        }
        assembly += "\t$labelOut:  \n"
        return assembly
    }
}