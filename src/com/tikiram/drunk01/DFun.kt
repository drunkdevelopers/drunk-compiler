package com.tikiram.drunk01

import java.util.*

class DFun
(
        line : Int,
        column: Int,
        val name: String,
        val parameters : List<SentenceLocalVar>,
        val sentences: List<Sentence>
) : Sentence(line, column)
{

    private val ambit = Ambit()

    private var localVarCount = 0
    private var retCount = 0

    override fun check(parentAmbit: Ambit, errors: MutableList<SemanticError>) {
        this.ambit.sets = parentAmbit.sets
        this.ambit.dFunctions = parentAmbit.dFunctions

        // *se valida cada sentencia
        // como la unicidad de las variables
        // se tiene que recibir el ambito, esta contiene los tipos y funciones
        // tambien se tiene que recibir la lista de errores
        // se valida cada senencia

        //Collections.reverse(parameters);
        for (s in parameters){
            s.isParameter = true
            s.check(ambit, errors)
        }

        var c = parameters.size

        while (c > 0){
            val sentence = parameters[c - 1]
            val existedVar = ambit.localVars[sentence.name]
            if (existedVar != null){

                sentence.position = retCount + 4
                ambit.localVars[sentence.name] = sentence

                // Siempre se va a pasar enteros y referencias (enteros)
                retCount += 2
                //retCount += type.size
            }
            c--
        }


        for (sentence in sentences){
            sentence.check(ambit, errors)
        }

        // se asignan las posiciones de las variables locales, y se obtiene el conteo de bytes utilizados en el stack
        for (sentence in sentences){
            if (sentence is SentenceLocalVar){
                val existedVar = ambit.localVars[sentence.name]
                if (existedVar != null){
                    ambit.localVars[sentence.name] = sentence
                    localVarCount += ambit.sets[sentence.type]!!.size * sentence.exprInteger.value
                    sentence.position = -localVarCount
                }
            }
        }

    }

    override fun generateAssembly(ambit: Ambit): String {


        var assembly = ""
        assembly += "$name proc \n"

        assembly += "\tpush bp \n"
        assembly += "\tmov bp, sp \n"

        sentences.filter { it is SentenceLocalVar }.forEach {
            assembly += it.generateAssembly(this.ambit)
        }

        assembly += "\tpush si \n"
        assembly += "\tpush di \n"
        assembly += "\tpush bx \n"

        sentences.filter { it !is SentenceLocalVar }.forEach {
            assembly += it.generateAssembly(this.ambit)
        }


        assembly += "\tpop bx \n"
        assembly += "\tpop di \n"
        assembly += "\tpop si \n"
        assembly += "\tadd sp, $localVarCount \n"
        assembly += "\tpop bp \n"
        assembly += "\tret $retCount \n"
        assembly += "$name endp \n"
        return assembly
    }

}